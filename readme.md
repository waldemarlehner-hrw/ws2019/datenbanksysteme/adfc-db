#Verein (ADFC-RV)

##Anwendung: Daten einer Radtour vervollständigen. 

### Prozessschritte: 

1. Eingabe eines Orts. 
2. Ausgabe einer Liste aller Touren, bei denen ein Tourenleiter oder der Starttermin fehlt und der eingegebene Ort im Titel oder der Beschreibung vorkommt. 
3. Auswahl einer der Touren aus der Liste. 
4. Das Startdatum der Tour kann eingetragen werden oder geändert werden. 
5. Wenn ein Tourenleiter fehlt, werden alle aktuellen Tourenleiter des Vereins ausgegeben. 
6. Auswahl eines der Tourenleiter für die Tour.