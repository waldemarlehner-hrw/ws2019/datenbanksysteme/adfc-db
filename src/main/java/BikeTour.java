import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;


public class BikeTour {
    String rid,name,description;
    Date date;
    int guidesCount;

    BikeTour(@NotNull String name, @NotNull String description, @NotNull String rid, @Nullable Date date, int guidesCount){
        this.name = name;
        this.description = description;
        this.rid = rid;
        this.date = date;
        this.guidesCount = guidesCount;
    }

}

