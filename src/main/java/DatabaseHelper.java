import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultInternal;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Quasi die "Schnittstelle" zur Datenbank
 */
public class DatabaseHelper {
    private ODatabaseSession db;

    public DatabaseHelper(@Nullable ODatabaseSession odbs){
        if(odbs == null)
            throw new IllegalArgumentException("odbs is null");
        this.db = odbs;
    }
    //Gebe alle Touren aus
    public void PrintAllTours(){
        //Gebe Name, Zeitpkt, Dauer, Distanz von allen Veranstaltungen zurück
        String query = "SELECT Name,Beschreibung,Zeitpunkt,Dauer,Distanz,Treffpunkt from Veranstaltung WHERE (Typ='radtour' OR Typ='radtour_und_kurs') ";
        OResultSet ors = db.query(query);

        StringJoiner sj = new StringJoiner("\n\n\n");

        while(ors.hasNext()){
            OResult result = ors.next();
            String name = result.getProperty("Name");
            String beschreibung = result.getProperty("Beschreibung");
            Date zeitpunkt_ = (Date)result.getProperty("Zeitpunkt");
            String zeitpunkt = (zeitpunkt_ != null)? new SimpleDateFormat("dd.MM.yyyy @ HH:mm").format(zeitpunkt_) : " Keine Angabe ";

            OResultInternal treffpkt_root = result.getProperty("Treffpunkt");
            String treffpunkt = treffpkt_root.getProperty("Bezeichnung");
            String dauer = ConvertTime(result.getProperty("Dauer"));
            String distanz = result.getProperty("Distanz")+" km";
            sj.add(String.format("~~~~\nName > %s\nBeschreibung > %s\nZeitpunkt > %s\nTreffpunkt > %s\nDauer > %s\nDistanz > %s\n~~~~",name,beschreibung,zeitpunkt,treffpunkt,dauer,distanz));
        }
        ors.close();
        System.out.println("Alle Einträge in der Datenbank\n\n"+sj.toString());

    }
    //Bekomme alle Unvollständigen Radtouren, welche im Titel oder in der Beschreibung einen gegebenen Suchstring besitzen. Wenn der Suchstring null ist, dann werden alle unvolständigen Radtouren zurückgegeben
    @NotNull
    public BikeTour[] GetIncompleteBikeTours(@Nullable String searchString){
        //Bekomme alle Vertices
        String query = "SELECT @rid AS id, Name, Beschreibung, Zeitpunkt, in(betreut).size() AS guidesCount FROM Veranstaltung WHERE (Typ='radtour' OR TYP='radtour_kurs') AND ( in('betreut').size() = 0 OR Zeitpunkt IS NULL)";
        OResultSet response;
        if(searchString != null){
            query += " AND ( Name LIKE '%?%' OR Beschreibung LIKE '%?%' )";
            response = db.query(query,searchString,searchString);
        }else{
            response = db.query(query);
        }
        List<BikeTour> tours = new LinkedList<>();
        while(response.hasNext()){
            OResult res = response.next();
            String name = res.getProperty("Name");
            String beschreibung = res.getProperty("Beschreibung");
            ORecordId rid = res.getProperty("id");
            int guidesCount = res.getProperty("guidesCount");
            Date timestamp = res.getProperty("Zeitpunkt");
            tours.add(new BikeTour(name,beschreibung,rid.toString(),timestamp,(int)guidesCount));
        }
        BikeTour[] returnArray = new BikeTour[tours.size()];
        tours.toArray(returnArray);
        return returnArray;
    }

    //Gebe alle mitgegebenen Touren aus. Zudem gibt es die Option einen Auswahl-Index mit auszugeben
    public void PrintBikeTours(@NotNull BikeTour[] tours, boolean printSelectIndices){
        StringJoiner sj = new StringJoiner("\n\n\n");
        sj.add("----");
        int i = 1;
        for (BikeTour tour : tours){
            StringJoiner sj_tour = new StringJoiner("\n");
            sj_tour.add((printSelectIndices)?("["+ i++ +"] "):(">"));
            sj_tour.add("Name > "+tour.name);
            sj_tour.add("Beschreibung > "+tour.description);
            sj_tour.add("Zeitpunkt > "+((tour.date == null)?"Keine Angabe":new SimpleDateFormat("HH:mm dd/MM/yyyy").format(tour.date)));
            sj_tour.add("Anzahl Betreuer > "+tour.guidesCount);
            sj.add(sj_tour.toString());
        }
        sj.add("----");
        System.out.println(sj.toString());
    }

    //Wandle eine Angabe in Minuten in Tage, Stunden und Minuten um
    @NotNull
    private String ConvertTime(int valueAsMinutes){
        boolean isNegative = valueAsMinutes < 0;
        int hrs = valueAsMinutes / 60;
        int mins = valueAsMinutes % 60;
        int days = hrs / 24;

        return ((isNegative) ? "- " : "") + ((days != 0) ? (days+" Tag(e) "):"") + ((hrs != 0) ? (hrs+" Stunde(n) ") : "") + ((mins!=0)?(mins+" Minute(n)"):"");

    }
    //Bekomme alle Tourenleiter
    public TourGuide[] GetTourGuides(@NotNull String tourId){
        String query = "SELECT expand(in('betreut')) as t FROM ?";
        return GenerateTourGuidesFromResultSet(db.query(query,tourId));

    }

    //Gebe eine Tour im Detail aus
    public void PrintTourDetailed(@NotNull BikeTour tour){
        StringJoiner sj = new StringJoiner("\n");
        sj.add("---------------------");
        sj.add("Name > "+tour.name);
        sj.add("Beschreibung > "+tour.description);
        sj.add("Zeitpunkt > "+((tour.date == null)?"Keine Angabe":new SimpleDateFormat("hh:mm dd/MM/yyyy")));
        sj.add("Anzahl Betreuer > "+tour.guidesCount);
        //Gebe alle Tourenleiter, welche zu dieser Tour gehören, aus
        if(tour.guidesCount > 0){
            TourGuide[] guides = GetTourGuides(tour.rid);
            sj.add("Betreuer > ");
            for(TourGuide guide : guides){
                sj.add(" > "+guide.firstname+" "+guide.lastname);
            }
        }
        sj.add("---------------------");
    }
    // Bekomme alle Mitglieder, welche die Aufgabe Tourenleiter übernehmen
    public TourGuide[] GetAllTourGuides(){
        String query = "SELECT expand(in(uebernimmt)) FROM Aufgabe WHERE Name = \"Tourenleiter\" AND inE(uebernimmt)[0].Enddatum IS NULL";
        return GenerateTourGuidesFromResultSet(db.query(query));
    }

    //Erstelle die Tourenleiter-Objekte aus der Antwort der Datenbank
    private TourGuide[] GenerateTourGuidesFromResultSet(@NotNull OResultSet ors){
        List<TourGuide> guides = new LinkedList<>();
        while(ors.hasNext()){
            OResult or = ors.next();
            String id, firstname, lastname;
            id = ((ORecordId)or.getProperty("@rid")).toString();
            OResultInternal oirs = or.getProperty("Name");
            firstname = oirs.getProperty("Vorname");
            lastname = oirs.getProperty("Nachname");
            guides.add(new TourGuide(firstname,lastname,id));
        }
        TourGuide[] returnArray = new TourGuide[guides.size()];
        guides.toArray(returnArray);
        return  returnArray;
    }

    //Aktualisiere das Datum einer Tour mit der mitgegebenen @rid
    public void UpdateDate(String rid, Date date){
        db.command(String.format("UPDATE Veranstaltung SET Zeitpunkt = DATE(%d) WHERE @rid = %s", date.getTime(), rid));
    }

    //Füge neue Tourenleiter zu einer Tour hinzu
    public void AddTourGuidesToTour(@NotNull BikeTour tour, @NotNull TourGuide[] guides) {
        for(TourGuide guide : guides){
           String query = "CREATE EDGE betreut FROM ? TO ?";
           db.command(query,guide.rid,tour.rid);
        }
    }
    //Entferne alle Tourenleiter von einer Tour
    public void RemoveAllGuidesFromTour(@NotNull BikeTour tour){
        String query = "DELETE EDGE betreut WHERE @rid IN (SELECT inE() FROM ?)";
        db.command(query, tour.rid);
    }
}
