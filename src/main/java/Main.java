import com.orientechnologies.orient.core.db.*;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.StringJoiner;


public class Main {
    static OrientDB orientdb;

    static ODatabaseSession session;
    static DatabaseHelper dbh;
    public static void main(String[] args){



        //Verbinde zur Datenbank
        try{
            orientdb = new OrientDB("remote:localhost",OrientDBConfig.defaultConfig());
            session =orientdb.open("root","admin","admin");
        }
        catch (Exception e){
            System.out.println("[FEHLER] Konnte keine Verbindung zur Datenbank herstellen.");
            e.printStackTrace();
            Close(-4);
        }
        if(session == null){
            System.out.println("[FEHLER] Konnte keine Instanz der Session aufbauen.");
            Close(-5);
        }
        if(orientdb == null){
            System.out.println("[FEHLER] Konnte keine Instanz der Datenbank aufbauen.");
            Close(-6);
        }



        //Erstelle die "Schnittstelle" zur Datenbank
        dbh = new DatabaseHelper(session);

        //Hauptmenü Loop
        mainmenu:
        while(true){
            System.out.println(
                    "Hauptmenü\n~~~~\n[1] > Alle Radtouren anzeigen\n[2] > Bearbeiten der Radtouren ohne Tourenleiter oder Starttermin\n\n[0] > Beenden\n~~~~\n\n"
            );
            Integer input = Input.GetIntegerFromRange(0,2,false);
            assert  input != null; // Check wegen @Nullable Annotation
            switch (input){
                case 0:
                    //Zurück zum Menü
                    break mainmenu;
                case 1:
                    //Gebe alle Touren aus
                    dbh.PrintAllTours();
                    Input.AwaitContinueInput();
                    break;
                case 2:
                    //Vervolständige Touren
                    EditTours();
                    break;
            }

        }

        Close(0);
    }
    //Beim Schließen den Programms sollen die Orient-Objekte geschlossen werden, um Speicher freizumachen
    public static void Close(int errcode){
        if(session != null)
            session.close();
        if(orientdb != null)
            orientdb.close();
        System.exit(errcode);
    }

    //Im Menü wurde das Bearbeiten von unvolständiges Touren gewählt. Es wird nach einem Suchstring gefragt und dann wird die Routine zum vervolständigen Ausgeführt
    private static void EditTours(){
        System.out.println(
            "Geben Sie einen Suchstring ein. Es werden alle Einträge mit fehlendem Tourenleiter bzw. " +
            "fehlendem Starttermin ausgeben, bei welchen der Suchstring im Namen oder der Beschreibung der " +
            "Radtour vorkommt. Eine leere Sucheingabe gibt alle unvollständigen Radtouren zurück."
        );
        String searchString = Input.GetString(true);
        while (true){
            //Bleibe in der Subroutine. Somit können mehrere Touren nacheinander Korrigiert werden
            BikeTour[] incompleteTours = dbh.GetIncompleteBikeTours(searchString);
            //Wenn alle Touren vollständig sind geht es zum Menü zurück
            if(incompleteTours.length == 0){
                System.out.println("Alle Touren sind vollständig");
                return;
            }
            BikeTour selectedTour = GetTourToEdit(incompleteTours);
            dbh.PrintTourDetailed(selectedTour);
            System.out.println("Operation auswählen\n----\n[0] > Zurück zum Hauptmenü\n[1] > Anfangszeitpunkt abändern\n[2] > Tourenleiter bearbeiten");
            Integer choice = Input.GetIntegerFromRange(0,2,false);
            assert choice != null;
            switch (choice){
                case 0:
                    //Gehe zurück zum Menü
                    return;
                case 1:
                    //Ändere Datum
                    ChangeDateOfTour(selectedTour);
                    break;
                case 2:
                    //Ändere die Tourenleiter
                    ChangeTourGuideOfTour(selectedTour);
                    break;
            }



        }


    }
    //Ändere die Tourenleiter die zu einer Tour dazugehören
    private static void ChangeTourGuideOfTour(BikeTour selectedTour) {
        System.out.println("\n\n\n\n");
        TourGuide[] guides = dbh.GetAllTourGuides();
        boolean[] isSelected = new boolean[guides.length];
        Arrays.fill(isSelected,false);
        int guidesCount = 0;
        while(true){
            System.out.println("Wählen Sie die Tourenleiter für diese Tour aus. 0 beendet die Auswahl.");
            PrintCurrentTourGuidesSelection(guides,isSelected);
            Integer choice = Input.GetIntegerFromRange(0,guides.length,false);
            assert choice != null;
            if(choice == 0){
                System.out.println("\n\n\n\nFolgende Tourenleiter wurden für die Tour ausgewählt:");
                guidesCount = 0;
                for(int i = 0; i < guides.length; i++){
                    if(isSelected[i]){
                        guidesCount++;
                        System.out.println("> "+guides[i].firstname+" "+guides[i].lastname);
                    }
                }
                if(guidesCount == 0){
                    System.out.println("Es wurden keine Tourenleiter ausgewählt.");
                }

                System.out.println("Stimmt die Auswahl?");
                if(Input.GetYesNo()){
                    break;
                }
                continue;

            }
            isSelected[choice-1] = !isSelected[choice-1];
        }

        dbh.RemoveAllGuidesFromTour(selectedTour);

        if(guidesCount == 0)
            return;
        TourGuide[] selectedGuides = new TourGuide[guidesCount];
        {
            int arri = 0;
            for (int i = 0; i < isSelected.length; i++) {
                if(isSelected[i]){
                    selectedGuides[arri++] = guides[i];
                }
            }
        }
        dbh.AddTourGuidesToTour(selectedTour,selectedGuides);



    }

    //Gebe die aktuelle Auswahl der Tourenleiter für eine zu vervollständigende Tour an
    private static void PrintCurrentTourGuidesSelection(TourGuide[] guides, boolean[] isSelected) {

        StringJoiner sj = new StringJoiner("\n");
        for(int i = 0;i<guides.length;i++){
            sj.add("["+(i+1)+"] ["+(isSelected[i] ? "X" : " ")+"] > "+guides[i].firstname+" "+guides[i].lastname);
        }
        System.out.println(sj.toString());
    }

    //Ändere den Startzeitpunkt einer Ausgewählten Tour
    private static void ChangeDateOfTour(BikeTour selectedTour) {
        System.out.println("\n\n\n\n");
        String previousDateAsString = (selectedTour.date == null) ? "nicht angegeben" : new SimpleDateFormat("HH:mm dd/MM/YYYY").format(selectedTour.date);
        System.out.println("Aktueller Zeitpunkt der Tour '"+selectedTour.name+"' ist "+previousDateAsString+"." );
        System.out.println("Wirklich abändern?");
        if(!Input.GetYesNo()){
            return;
        }
        Date newDate = Input.GetNewTime();
        dbh.UpdateDate(selectedTour.rid,newDate);
        selectedTour.date = newDate;
    }

    //Bekomme eine Tour zum Bearbeiten durch Nutzereingaben zurück
    @NotNull
    private static BikeTour GetTourToEdit(@NotNull BikeTour[] tours){


        dbh.PrintBikeTours(tours,true);
        if(tours.length > 1){
            System.out.println("Wählen Sie eine unvolständige Radtour aus");
            Integer choice = Input.GetIntegerFromRange(1,tours.length,false);
            assert choice != null;
            return tours[--choice];
        }
        System.out.println("Es gibt nur eine Tour zum Auswählen. Wähle diese Automatisch aus.");
        return tours[0];

    }




}
