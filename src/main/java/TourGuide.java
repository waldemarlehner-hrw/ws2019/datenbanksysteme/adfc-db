import org.jetbrains.annotations.NotNull;

public class TourGuide {
    String firstname, lastname, rid;
    TourGuide(@NotNull String firstname, @NotNull String lastname, @NotNull String rid){
        this.firstname = firstname;
        this.lastname = lastname;
        this.rid = rid;
    }
}
